## Packages:

I use endeavouros btw
```bash
pacman -S picom i3 zsh rofi fastfetch kitty polybar flameshot cbonsai pipes.sh 
```
## Installation:

```bash
git clone https://gitlab.com/macksom/dotfiles
cd dotfiles
cp -r i3/ polybar/ kitty/ ~/.config/
``` 
# Screenshots
![image.png](rice/image.png)
